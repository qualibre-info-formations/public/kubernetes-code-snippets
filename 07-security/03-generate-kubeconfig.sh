kubectl config --kubeconfig=jane.kubeconfig set-cluster my-cluster --server=https://kubernetes.docker.internal:6443 --certificate-authority=/mnt/c/Users/[USER]/AppData/Local/Docker/pki/ca.crt
kubectl config --kubeconfig=jane.kubeconfig set-credentials jane --client-certificate=jane.crt --client-key=jane.key
kubectl config --kubeconfig=jane.kubeconfig set-context jane-context --cluster=my-cluster --namespace=default --user=jane
kubectl config --kubeconfig=jane.kubeconfig use-context jane-context