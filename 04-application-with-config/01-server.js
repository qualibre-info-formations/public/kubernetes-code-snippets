const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const message = process.env.WELCOME_MESSAGE || 'Bienvenue sur mon application!';

app.get('/', (req, res) => {
  res.send(message);
});

app.listen(port, () => {
  console.log(`Application démarrée sur http://localhost:${port}`);
});
