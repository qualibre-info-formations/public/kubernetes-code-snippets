MINIKUBE_VERSION="1.31.2"
wget https://github.com/kubernetes/minikube/releases/download/v${MINIKUBE_VERSION}/minikube_${MINIKUBE_VERSION}-0_amd64.deb
sudo apt install ./minikube_${MINIKUBE_VERSION}-0_amd64.deb
rm -f minikube_${MINIKUBE_VERSION}-0_amd64.deb