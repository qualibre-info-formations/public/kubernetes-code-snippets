sudo apt install conntrack
CRICTL_VERSION="1.28.0"
wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v${CRICTL_VERSION}/crictl-v${CRICTL_VERSION}-linux-amd64.tar.gz
sudo tar zxvf crictl-v${CRICTL_VERSION}-linux-amd64.tar.gz -C /usr/local/bin/
rm -f crictl-v${CRICTL_VERSION}-linux-amd64.tar.gz